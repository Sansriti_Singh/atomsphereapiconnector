package com.boomi.connector.AtomSphereAPI;

import com.boomi.connector.api.DeleteRequest;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.util.BaseDeleteOperation;

public class AtomSphereAPIDeleteOperation extends BaseDeleteOperation {

	protected AtomSphereAPIDeleteOperation(AtomSphereAPIConnection conn) {
		super(conn);
	}

	@Override
	protected void executeDelete(DeleteRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
    public AtomSphereAPIConnection getConnection() {
        return (AtomSphereAPIConnection) super.getConnection();
    }
}