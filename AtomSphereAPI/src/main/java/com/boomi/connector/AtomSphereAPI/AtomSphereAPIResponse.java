package com.boomi.connector.AtomSphereAPI;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.List;

import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.OperationType;

public class AtomSphereAPIResponse {
	
	HttpURLConnection huc;
	
	enum GETTYPE {
		Account,
		Atom,
		Cloud
	  }

	public AtomSphereAPIResponse(HttpURLConnection openConnection, String username, String password) {
		huc =  openConnection;
		String usernameColonPassword = username+":"+password;
		String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());
		//Include the HTTP Basic Authentication payload
		huc.addRequestProperty("Authorization", basicAuthPayload);
	}

	public AtomSphereAPIResponse(HttpURLConnection send) {
		huc = send;
	}
	
	public int getResponseCode() {
		int responseCode=0;
		try {
			responseCode = huc.getResponseCode();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		return responseCode;
	}

	public String getResponseMessage() {
		String responseMessage = "";
		try {
			responseMessage = huc.getResponseMessage();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		return responseMessage;
	
	}

	public String getResponseCodeAsString() {
		
				String responseCode="";
				try {
					responseCode = Integer.toString(huc.getResponseCode());
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				
				return responseCode;
	}

	public InputStream getResponse() {
		
				InputStream is = null;
				try {
					
					 is = huc.getInputStream();
					
				} catch (IOException e) {
					
					e.printStackTrace();
				}
				return is;
	}
	
	/*public InputStream getResponseXML() throws IOException {
		
		InputStream is = null;
		is = new ByteArrayInputStream("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><Root><Element name = \"Account\"/></Root>".getBytes());
		return is;
}*/

	public OperationStatus getStatus() {
		
				return OperationStatus.SUCCESS;
	}

	public void disconnect() {
		huc.disconnect();
		
	}
	
	public List<String> getEnum(OperationType operationType) {
		List<String> typeList = new ArrayList<String>();
		
		for (GETTYPE myVar : GETTYPE.values()) {
			typeList.add(myVar.toString());
			}
		
		return typeList;
	}

	public InputStream getResponseXSD() throws IOException{
		InputStream is = null;
			//File file = new File("C:\\Users\\sansriti_singh\\eclipse-workspace\\TestJUnit\\Account.xsd");
			//is = new FileInputStream(file);
			  is = new ByteArrayInputStream("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:bns=\"http://api.platform.boomi.com/\" xmlns:tns=\"http://api.platform.boomi.com/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" elementFormDefault=\"qualified\" targetNamespace=\"http://api.platform.boomi.com/\" version=\"1.0\"><xs:element name=\"Account\" type=\"tns:Account\"/><xs:complexType name=\"Account\"><xs:annotation><xs:appinfo><filter ignore=\"true\" xmlns=\"http://www.boomi.com/connector/annotation\"><field name=\"accountId\"/><field name=\"dateCreated\"/><field name=\"expirationDate\"/><field name=\"name\"/><field name=\"status\"/><field name=\"widgetAccount\"/></filter></xs:appinfo></xs:annotation><xs:complexContent><xs:extension base=\"tns:BaseType\"><xs:sequence><xs:element minOccurs=\"0\" name=\"licensing\"><xs:complexType><xs:sequence><xs:element minOccurs=\"0\" name=\"standard\" type=\"tns:License\"/><xs:element minOccurs=\"0\" name=\"smallBusiness\" type=\"tns:License\"/><xs:element minOccurs=\"0\" name=\"enterprise\" type=\"tns:License\"/><xs:element minOccurs=\"0\" name=\"tradingPartner\" type=\"tns:License\"/></xs:sequence></xs:complexType></xs:element></xs:sequence><xs:attribute name=\"accountId\" type=\"xs:string\"/><xs:attribute name=\"name\" type=\"xs:string\"/><xs:attribute name=\"status\" type=\"tns:AccountStatus\"/><xs:attribute name=\"dateCreated\" type=\"xs:string\"/><xs:attribute name=\"expirationDate\" type=\"xs:string\"/><xs:attribute name=\"widgetAccount\" type=\"xs:boolean\"/><xs:attribute name=\"suggestionsEnabled\" type=\"xs:boolean\"/><xs:attribute name=\"supportAccess\" type=\"xs:boolean\"/><xs:attribute name=\"supportLevel\" type=\"tns:AccountSupportLevel\"/></xs:extension></xs:complexContent></xs:complexType><xs:complexType name=\"License\"><xs:sequence/><xs:attribute name=\"purchased\" type=\"xs:int\"/><xs:attribute name=\"used\" type=\"xs:int\"/></xs:complexType><xs:complexType name=\"BaseType\"><xs:sequence/></xs:complexType><xs:simpleType name=\"AccountSupportLevel\"><xs:restriction base=\"xs:string\"><xs:enumeration value=\"standard\"/><xs:enumeration value=\"premier\"/></xs:restriction></xs:simpleType><xs:simpleType name=\"AccountStatus\"><xs:restriction base=\"xs:string\"><xs:enumeration value=\"trial\"/><xs:enumeration value=\"active\"/><xs:enumeration value=\"suspended\"/><xs:enumeration value=\"deleted\"/><xs:enumeration value=\"unlimited\"/></xs:restriction></xs:simpleType></xs:schema>".getBytes());
			return is;
	}

}
