package com.boomi.connector.AtomSphereAPI;

import java.io.Closeable;
import java.io.InputStream;

import com.boomi.connector.AtomSphereAPI.AtomSphereAPIResponse;
import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.ObjectIdData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.util.BaseGetOperation;
import com.boomi.util.IOUtil;

public class AtomSphereAPIGetOperation extends BaseGetOperation {

    protected AtomSphereAPIGetOperation(AtomSphereAPIConnection conn) {
        super(conn);
    }

	@Override
	protected void executeGet(GetRequest request, OperationResponse response) {
		ObjectIdData input = request.getObjectId();

		try {
			AtomSphereAPIResponse resp = this.getConnection().doGet(this.getContext().getObjectTypeId(), input.getObjectId());
			if (resp.getResponseCode() == 404) {
				System.out.println("invalid url");
				response.addEmptyResult(input, OperationStatus.SUCCESS, resp.getResponseCodeAsString(),
						resp.getResponseMessage());
			} else {
				InputStream obj = null;

				try {
					obj = resp.getResponse();
					if (obj != null) {
						response.addResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
								resp.getResponseMessage(), ResponseUtil.toPayload(obj));
					} else {
						response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
								resp.getResponseMessage());
					}
				} finally {
					IOUtil.closeQuietly(new Closeable[]{obj});
				}
			}
		} catch (Exception var10) {
			ResponseUtil.addExceptionFailure(response, input, var10);
		}
	}

	@Override
    public AtomSphereAPIConnection getConnection() {
        return (AtomSphereAPIConnection) super.getConnection();
    }
}