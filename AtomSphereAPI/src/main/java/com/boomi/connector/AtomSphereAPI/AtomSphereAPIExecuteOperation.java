package com.boomi.connector.AtomSphereAPI;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;

public class AtomSphereAPIExecuteOperation extends BaseUpdateOperation {

	protected AtomSphereAPIExecuteOperation(AtomSphereAPIConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		// TODO Auto-generated method stub
	}

	@Override
    public AtomSphereAPIConnection getConnection() {
        return (AtomSphereAPIConnection) super.getConnection();
    }
}