package com.boomi.connector.AtomSphereAPI;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.boomi.connector.AtomSphereAPI.AtomSphereAPIResponse;
import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.api.PropertyMap;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.DOMUtil;
import com.boomi.util.IOUtil;
import com.boomi.util.StreamUtil;
import com.boomi.util.URLUtil;

public class AtomSphereAPIConnection extends BaseConnection {
	
	private static final List<Entry<String, String>> UPDATE_OUTPUT_PARAMS = Collections.unmodifiableList(new ArrayList(Collections.singletonMap("type", "json").entrySet()));
	private final String _baseUrl;
	private final String _username;
	private final String _password;
	private OperationType operationType;

	public AtomSphereAPIConnection(BrowseContext context) {
		super(context);
		this._baseUrl = getBaseUrl(context.getConnectionProperties());
		this._username = getUserName(context.getConnectionProperties());
		this._password = getPassword(context.getConnectionProperties());
		operationType = context.getOperationType();
	}
	
	public Document getMetadata(String objectTypeId) throws IOException, SAXException, ParserConfigurationException {
		//URL url = objectTypeId != null
		//		? buildUrl(this._baseUrl, "metadata", objectTypeId)
				//: buildUrl(this._baseUrl, "metadata");
		//		: buildUrl(this._baseUrl+"xsd=1");
				URL url = buildUrl(this._baseUrl,objectTypeId+"?xsd=1");
				//URL url = buildUrl(this._baseUrl);
		AtomSphereAPIResponse resp = new AtomSphereAPIResponse((HttpURLConnection) url.openConnection(), _username, _password);
		//System.out.println(parse(resp.getResponse()));
		return parse(resp.getResponseXSD());
	}

	
	public AtomSphereAPIResponse doGet(String objectType, String objectId) throws IOException {
		String doGeturl = this._baseUrl;// + objectType + objectId;
		URL url = buildUrl(doGeturl, objectType, objectId);
		//URL url = new URL(doGeturl);
		return new AtomSphereAPIResponse((HttpURLConnection) url.openConnection(), _username, _password);
	}


	public AtomSphereAPIResponse doCreate(String objectType, InputStream data) throws IOException {
		String doCreateurl = this._baseUrl;
		return new AtomSphereAPIResponse(
				//send(buildUrl(UPDATE_OUTPUT_PARAMS, doCreateurl, objectType), "POST", "application/json", data));
				send(buildUrl(UPDATE_OUTPUT_PARAMS,doCreateurl), "POST", "application/json", data, _username, _password));
	}

	
	private static String getBaseUrl(PropertyMap props) {
		return props.getProperty("url");
	}
	
	private String getUserName(PropertyMap props) {
		return props.getProperty("username");
	}
	
	private String getPassword(PropertyMap props) {
		return props.getProperty("password");
	}

	private static Document parse(InputStream input) throws SAXException, IOException, ParserConfigurationException {
		Document doc;
		try {
			//var1 = DOMUtil.newDocumentBuilderNS().parse(input);
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            doc = docBuilder.parse(input);
		} finally {
			IOUtil.closeQuietly(new Closeable[]{input});
		}

		return doc;
	}

	private static URL buildUrl(String... components) throws IOException {
		return buildUrl((List) null, components);
		
	}

	private static URL buildUrl(List<Entry<String, String>> params, String... components) throws IOException {
		return URLUtil.makeUrl(params, (Object[]) components);
	}

	private static HttpURLConnection send(URL url, String requestMethod, String contentType, InputStream data, String _username, String _password)
			throws IOException {
		HttpURLConnection var6;
		try {
			HttpURLConnection conn = prepareSend(url, requestMethod, contentType);
			String usernameColonPassword =_username+":"+_password;
			String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());
			//Include the HTTP Basic Authentication payload
			conn.addRequestProperty("Authorization", basicAuthPayload);
			OutputStream out = conn.getOutputStream();

			try {
				StreamUtil.copy(data, out);
			} finally {
				out.close();
			}

			var6 = conn;
		} finally {
			IOUtil.closeQuietly(new Closeable[]{data});
		}

		return var6;
	}

	private static HttpURLConnection prepareSend(URL url, String requestMethod, String contentType) throws IOException {
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod(requestMethod);
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", contentType);
		return conn;
	}
	
	public AtomSphereAPIResponse doQuery(String objectTypeId) throws IOException {
		String doGeturl = this._baseUrl;
		//URL url = buildUrl(doGeturl, objectType, objectId);
		URL url = new URL(doGeturl);
		return new AtomSphereAPIResponse((HttpURLConnection) url.openConnection(), _username, _password);
		
	}
	
	public List<String> getObjectTypes() throws IOException, SAXException, ParserConfigurationException {
		URL url = buildUrl(this._baseUrl+"xsd=1");
		AtomSphereAPIResponse resp = new AtomSphereAPIResponse((HttpURLConnection) url.openConnection(), _username, _password);
		//System.out.println(parse(resp.getResponse()));
		return resp.getEnum(operationType);
	}
	
}