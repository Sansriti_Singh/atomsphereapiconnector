package com.boomi.connector.AtomSphereAPI;

import java.util.Collection;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.util.BaseBrowser;

public class AtomSphereAPIBrowser extends BaseBrowser {
	
	private static final String TYPE_ELEMENT = "type";

    protected AtomSphereAPIBrowser(AtomSphereAPIConnection conn) {
        super(conn);
    }

	
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId,
			Collection<ObjectDefinitionRole> roles) {
		try {
			Document defDoc = this.getConnection().getMetadata(objectTypeId);
			ObjectDefinitions defs = new ObjectDefinitions();
			ObjectDefinition def = new ObjectDefinition();
			def.setSchema(defDoc.getDocumentElement());
			def.setElementName(objectTypeId);
			defs.getDefinitions().add(def);
			return defs;
		} catch (Exception var6) {
			throw new ConnectorException(var6);
		}
	}

	//@Override
	/*
	 * public ObjectTypes getObjectTypes() { try { Document typeDoc =
	 * this.getConnection().getMetadata((String) null); NodeList typeList =
	 * typeDoc.getElementsByTagName("xs:element"); ObjectTypes types = new
	 * ObjectTypes();
	 * 
	 * for (int i = 0; i < typeList.getLength(); ++i) { Element typeEl = (Element)
	 * typeList.item(i); //String typeName = typeEl.getTextContent().trim(); String
	 * typeName = typeEl.getAttribute("type").trim(); ObjectType type = new
	 * ObjectType(); type.setId(typeName); types.getTypes().add(type); }
	 * 
	 * return types; } catch (Exception var8) { throw new ConnectorException(var8);
	 * } }
	 */
	
	public ObjectTypes getObjectTypes() {
		try {
			List<String> typeList = this.getConnection().getObjectTypes();
			ObjectTypes types = new ObjectTypes();

			for (int i = 0; i < typeList.size(); ++i) {
				String typeName = typeList.get(i);
				ObjectType type = new ObjectType();
				type.setId(typeName);
				types.getTypes().add(type);
			}

			return types;
		} catch (Exception var8) {
			throw new ConnectorException(var8);
		}
	}
	
	@Override
    public AtomSphereAPIConnection getConnection() {
        return (AtomSphereAPIConnection) super.getConnection();
    }
}