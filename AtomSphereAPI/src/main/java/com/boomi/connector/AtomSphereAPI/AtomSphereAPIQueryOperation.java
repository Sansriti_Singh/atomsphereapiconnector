package com.boomi.connector.AtomSphereAPI;

import java.io.Closeable;
import java.io.InputStream;

import com.boomi.connector.AtomSphereAPI.AtomSphereAPIResponse;
import com.boomi.connector.api.FilterData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.OperationStatus;
import com.boomi.connector.api.QueryFilter;
import com.boomi.connector.api.QueryRequest;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.util.BaseQueryOperation;
import com.boomi.util.IOUtil;

public class AtomSphereAPIQueryOperation extends BaseQueryOperation {

	protected AtomSphereAPIQueryOperation(AtomSphereAPIConnection conn) {
		super(conn);
	}

	@Override
	protected void executeQuery(QueryRequest request, OperationResponse response) {
		// grab request information
        FilterData requestData = request.getFilter();

        QueryFilter filter = requestData.getFilter();

         if((filter == null) || (filter.getExpression() == null)) {

        	 try {
        		 AtomSphereAPIResponse resp = this.getConnection().doQuery(this.getContext().getObjectTypeId());
     			
     			if (resp.getResponseCode() == 404) {
     				System.out.println("invalid url");
     				response.addEmptyResult(requestData,OperationStatus.SUCCESS, resp.getResponseCodeAsString(),
     						resp.getResponseMessage());
     			} else {
     				InputStream obj = null;

     				try {
     					obj = resp.getResponse();
     					if (obj != null) {
     						response.addResult(requestData, resp.getStatus(), resp.getResponseCodeAsString(),
     								resp.getResponseMessage(), ResponseUtil.toPayload(obj));
     					} else {
     						response.addEmptyResult(requestData, resp.getStatus(), resp.getResponseCodeAsString(),
     								resp.getResponseMessage());
     					}
     				} finally {
     					IOUtil.closeQuietly(new Closeable[]{obj});
     				}
     			}
     		} catch (Exception var10) {
     			ResponseUtil.addExceptionFailure(response, requestData, var10);
     		}
        	 
         }
	}

	@Override
    public AtomSphereAPIConnection getConnection() {
        return (AtomSphereAPIConnection) super.getConnection();
    }
}