package com.boomi.connector.AtomSphereAPI;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.Browser;
import com.boomi.connector.api.Operation;
import com.boomi.connector.api.OperationContext;
import com.boomi.connector.util.BaseConnector;

public class AtomSphereAPIConnector extends BaseConnector {

    @Override
    public Browser createBrowser(BrowseContext context) {
        return new AtomSphereAPIBrowser(createConnection(context));
    }    

    @Override
    protected Operation createGetOperation(OperationContext context) {
        return new AtomSphereAPIGetOperation(createConnection(context));
    }

    @Override
    protected Operation createQueryOperation(OperationContext context) {
        return new AtomSphereAPIQueryOperation(createConnection(context));
    }

    @Override
    protected Operation createCreateOperation(OperationContext context) {
        return new AtomSphereAPICreateOperation(createConnection(context));
    }

    @Override
    protected Operation createUpdateOperation(OperationContext context) {
        return new AtomSphereAPIUpdateOperation(createConnection(context));
    }

    @Override
    protected Operation createUpsertOperation(OperationContext context) {
        return new AtomSphereAPIUpsertOperation(createConnection(context));
    }

    @Override
    protected Operation createDeleteOperation(OperationContext context) {
        return new AtomSphereAPIDeleteOperation(createConnection(context));
    }

    @Override
    protected Operation createExecuteOperation(OperationContext context) {
        return new AtomSphereAPIExecuteOperation(createConnection(context));
    }
   
    private AtomSphereAPIConnection createConnection(BrowseContext context) {
        return new AtomSphereAPIConnection(context);
    }
}