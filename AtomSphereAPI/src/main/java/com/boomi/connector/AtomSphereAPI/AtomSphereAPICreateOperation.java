package com.boomi.connector.AtomSphereAPI;

import java.io.Closeable;
import java.io.InputStream;
import java.util.Iterator;

import com.boomi.connector.AtomSphereAPI.AtomSphereAPIResponse;
import com.boomi.connector.api.ObjectData;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.util.BaseUpdateOperation;
import com.boomi.util.IOUtil;

public class AtomSphereAPICreateOperation extends BaseUpdateOperation {

	protected AtomSphereAPICreateOperation(AtomSphereAPIConnection conn) {
		super(conn);
	}

	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		String objectType = this.getContext().getObjectTypeId();
		Iterator i$ = request.iterator();
		
		while (i$.hasNext()) {
			ObjectData input = (ObjectData) i$.next();

			try {
				AtomSphereAPIResponse resp = this.getConnection().doCreate(objectType, input.getData());
				InputStream obj = null;

				try {
					obj = resp.getResponse();
					if (obj != null) {
						response.addResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
								resp.getResponseMessage(), ResponseUtil.toPayload(obj));
					} else {
						response.addEmptyResult(input, resp.getStatus(), resp.getResponseCodeAsString(),
								resp.getResponseMessage());
					}
				} finally {
					IOUtil.closeQuietly(new Closeable[]{obj});
				}
			} catch (Exception var12) {
				ResponseUtil.addExceptionFailure(response, input, var12);
			}
		}
	}

	@Override
    public AtomSphereAPIConnection getConnection() {
        return (AtomSphereAPIConnection) super.getConnection();
    }
}