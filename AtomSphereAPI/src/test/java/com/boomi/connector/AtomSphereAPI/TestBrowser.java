package com.boomi.connector.AtomSphereAPI;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import com.boomi.connector.AtomSphereAPI.AtomSphereAPIConnector;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.testutil.ConnectorTester;
import com.boomi.connector.testutil.SimpleOperationResult;

class TestBrowser {

	@Test
	public void testBrowserBoomiConnector()
    {
        long start=System.currentTimeMillis();
        AtomSphereAPIConnector connector = new AtomSphereAPIConnector();
        ConnectorTester tester = new ConnectorTester(connector);
        Map<String, Object> connProps = new HashMap<String,Object>();
        connProps.put("url", "https://api.boomi.com/api/rest/v1/trainingsansritisingh-TYBV7S");
        connProps.put("username", "BOOMI_TOKEN.sansriti.singh@dell.com");
        connProps.put("password", "4446f783-0db6-4594-8cf4-8651d8144715");
        Map<String, Object> opProps = new HashMap<String,Object>();
        tester.setOperationContext(OperationType.GET, connProps, opProps,null,null);
        List<InputStream> inputs = new ArrayList<InputStream>();
        inputs.add(new ByteArrayInputStream("HELLO from JUNIT".getBytes()));
        List<SimpleOperationResult> actual = tester.executeGetOperation("trainingsansritisingh-TYBV7S");
        assertEquals("200", actual.get(0).getStatusCode());
        assertEquals("OK", actual.get(0).getMessage());
        System.out.println("Total Time"+(System.currentTimeMillis()-start));
        System.out.println("Payload:"+ actual.toString());
                
        //String profiles = tester.browseProfiles("Account");
        //String profiles = tester.browseTypes();
        //System.out.println(profiles);
        
    }

}
